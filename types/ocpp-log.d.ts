export interface OcppLogInfo {
    command: string;
    success: boolean;
    request: unknown;
    response?: unknown;
    date?: string;
    chargerId: string;
    requestId?: string;
}
