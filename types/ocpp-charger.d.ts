import { Address } from "./address";

export interface OcppChargerInfo {
    customerId: string;
    name: string;
    address: Address;
    heartbeatOn: string;
    characteristics: Characteristics;
    connector1Status: ConnectorStatus;
    connector2Status: ConnectorStatus;
    id: string;
    registerOn: string;
    isPublic: boolean;
}

interface ConnectorStatus {
    status: string;
    error: string;
    updatedOn?: string;
}

interface Characteristics {
    vendor: string;
    model: string;
    serialNumber?: string;
    firmwareVersion?: string;
}
