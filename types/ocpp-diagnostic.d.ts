export interface OcppDiagnosticsInfo {
    type: string;
    url: string;
    description: string;
    label?: string;
    registerOn?: string;
}