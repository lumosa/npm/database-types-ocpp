export interface ApiKeysInfo {
    id: string;
    key: string;
    project: string;
    expiresOn: string;
    registerOn?: string;
    roles?: string[];
}